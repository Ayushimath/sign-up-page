function validation() {
    let fname = document.getElementById("fname").value;
    let lname = document.getElementById("lname").value;
    let email = document.getElementById("email").value;
    let password = document.getElementById("pwd").value;
    let repeat_password = document.getElementById("repeat_pwd").value;
    let checkbox = document.getElementById("terms-conditions").checked;

    //all the p tags

    // let submitOK = "true";
    let splittedFname = fname.split("");
    let checkfname = true
    splittedFname.map((element) => {
        if (!(isNaN(element))) {
            checkfname = false;

        }
    })
    console.log(checkfname);
    if ((!checkfname) || (fname === "")) {
        checkfname = false;
        let p_firstname = document.getElementById("firstname-val");
        p_firstname.innerText = "* Enter a valid firstname";
        p_firstname.style.color = "red";


    }


    // Lastname check
    let splittedLname = lname.split("");
    let checklname = true;
    splittedLname.map((element) => {
        if (!(isNaN(element))) {
            checklname = false;
        }
    })


    //last name validation 
    if ((!checklname) || (lname === "")) {
        checklname = false;
        let p_lastname = document.getElementById("lastname-val");

        p_lastname.innerText = "* Enter a valid lastname";
        p_lastname.style.color = "red";


    }


    //email validation
    let validEmail = true
    if (!(email.includes("@gmail.com"))) {
        validEmail = false;
    }


    //email validation 

    if (!validEmail || email === "") {
        validEmail = false;
        let p_email = document.getElementById("email-val");

        p_email.innerText = "* Enter a valid email address";
        p_email.style.color = "red";


    }

    //passoword validation
    //6 min and max 12
    //one capital char
    //one small
    //one num
    //include spl
    spl = "~!@#$%^&*";
    let pwdArray = [0, 0, 0, 0];
    let splittedpwd = password.split("");
    splittedpwd.map((element) => {
        if (isNaN(element)) {
            if (element === element.toUpperCase()) {
                pwdArray[1] = 1;
            }
            if (element === element.toLowerCase()) {
                pwdArray[2] = 1;
            }

        } else {
            pwdArray[0] = 1;
        }
        if (spl.includes(element)) {
            pwdArray[3] = 1;
        }

    });
    pwd_val_sum = pwdArray.reduce((sum, element) => sum + element);
    let pwd_val = true;
    if (((password.length < 6 || password.length > 12)) || pwd_val_sum < 4) {
        pwd_val = false;
    }


    //password validation
    if ((!pwd_val) || password === "") {
        pwd_val = false;
        let p_pwd = document.getElementById("pwd-val");

        p_pwd.innerText = "* Enter a valid password";
        p_pwd.style.color = "red";
    }




    //Repeat password
    let repeatPwd_val = true;
    if (password !== repeat_password) {
        repeatPwd_val = false;

    }
    //repeat password function
    if ((!repeatPwd_val) || repeat_password === "") {
        repeatPwd_val = false;
        let p_repeat_pwd = document.getElementById("repeat-pwd-val");

        p_repeat_pwd.innerText = "* re-enter the password";
        p_repeat_pwd.style.color = "red";
    }

    //checkbox validation(its already a boolean value)
    if (!checkbox) {
        let p_checkbox = document.getElementById("checkbox-val");

        p_checkbox.innerText = "* check the terms and conditions";
        p_checkbox.style.color = "red";
    }



    if (checkfname && checklname && validEmail && pwd_val && repeatPwd_val && checkbox) {
        let success_msg = document.getElementById("success-msg");
        success_msg.innerText = "Thanks For Registration!!";
        success_msg.style.color = "green";
        success_msg.style.textAlign = "center";

        //set all values to blank
        document.getElementById("fname").value = "";
        document.getElementById("lname").value = "";
        document.getElementById("email").value = "";
        document.getElementById("pwd").value = "";
        document.getElementById("repeat_pwd").value = "";
        document.getElementById("terms-conditions").checked = false;

        //button disabled
        document.getElementById("btn").disabled = true;
    }

    console.log(fname, lname, email, password, repeat_password, checkbox);
}

